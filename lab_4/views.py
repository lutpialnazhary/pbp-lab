from django.shortcuts import render
from django.http.response import HttpResponse
from django.views.generic import ListView
from .models import Note
from .forms import NoteForm
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect

# Create your views here.
@login_required(login_url="/admin/login")
def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url="/admin/login")
def add_note(request):
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab_4')
    context = {'form':form}
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)
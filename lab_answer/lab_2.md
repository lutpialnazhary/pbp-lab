1. Apakah perbedaaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?


1. JSON adalah turunan JavaScript yang digunakan dalam transfer dan penyimpanan data. Kekinian, bahasa ini sering dimanfaatkan dalam pembuatan aplikasi web. JavaScript object notation atau JSON adalah format yang digunakan untuk menyimpan dan mentransfer data.

XML(Extensive Markup Language) adalah Bahasa markup yang diciptakan oleh konsorsium World Wide Web (W3C). Bahasa ini berfungsi untuk menyederhanakan proses penyimpanan dan pengiriman data antarserver. Singkatnya, XML itu dapat membantu User memahami sebuah struktur dasar suatu Dokumen atau halaman website.


        Berbeda dengan XML (extensive markup language) dan format lainnya yang memiliki fungsi serupa, JSON memiliki struktur data yang sederhana dan mudah dipahami. Itulah mengapa JSON sering digunakan pada API.
   
        JSON tidak hanya bisa digunakan oleh JavaScripts saja tetapi bisa juga digunakan oleh bahasa pemrograman PHP, Python, Ruby, C++, Perl

        Fungsi JSON kurang lebih sama dengan XML. Sebelum JSON diciptakan, format bahasa markup ini sering digunakan dengan JavaScript dalam AJAX (asynchronous JavaScript and XML) untuk penyimpanan dan perpindahan data.

        JSON biasanya digunakan untuk menyimpan sebuah informasi dengan cara yang terorganisir dan mudah diakses. 

        JSON hanyalah sebuah format data sedangkan XML adalah bahasa markup. XML digunakan untuk menyimpan dan mengangut suatu data dari satu aplikasi ke aplikasi lain dengan malalui Internet. JSON adalah format pertukaran data yang ringan sangat jauh lebih mudah untuk suatu komputer melakukan pengiriman sebuah data.

            Format JSON mengeluarkan suatu gambaran diri sendiri dan secara komparatif jauh leih mudah dibaca daripada XML. Tetapi, jika memerlukan suatu markup dokumen dan informasi metadata lebih baik menggunakan XML


2. HTML (Hypertext Markup Language) adalah sebuah bahasa yang digunakan untuk membuat halaman Web. Kode HTML tersebut memastikan format teks dan gambar yang tepat untuk browser Internet. Tanpa HTML, browser tidak akan tahu bagaimana menampilkan teks sebagai elemen atau memuat gambar atau elemen lainnya. 

lebih tepatnya bahasa standar Pemrograman yang digunakan untuk membuat halaman website yang dapat diakses melalui Internet 

Keuntungan yang kita dapatkan menggunakan HTML adalah 
    1.Tampilan suatu browser untuk dokumen HTML mudah dibuat
    2.Bahasa HTML mudah dipahami bagi semua orang ( sederhana)
    3.Dapat menggunakan banyak tag untuk membuat halaman
    4.HTML juga digunakan untuk berbagai kegunaan warna, objek dll

Kerugian yang kita dapatkan menggunakan HTML adalah
    1. HTML tidak mempunyai pemeriksaan Sintaks
    2. HTML tidak dapat digunakan sebagai pertukaran Data ( seperti halnya XML)
    3. HTML tidak berorientasi pada objek, jadi HTML bukan bahasa yang dapat kita kembangkan dan sangat tidak stabil.


Keuntungan yang kita dapatkan menggunakan XML adalah
    1.XML digunakan untuk membuat dokumen yang dapat dibawa ke berbagai lintas sistem dan aplikasi
    2. XML membantu untuk dapat bertukar data berbagai platform
    3. XML digunakan untuk memisahkan data dari HTML
    4. XML juga digunakan untuk menyederhanakan proses Perubahan Platform

Kerugian yang kita dapatkan menggunakan XML adalah
    1.XML harus membutuhkan sebuah aplikasi pemrosesan data.
    2.Sintaks XML biasanya terdapat penggunaan kembali sintaks yang dibuat.

jadi, XML dan HTML memiliki banyak keuntungan serta Kerugian bagi user 

 seperti : HTML banyak digunakan untuk membuat tampilan untuk halaman website dan banyak hal mengenai cara-cara menampilkan sebuah informasi atau data, sedangkan XML disini lebih banyak berperan sebagai transport data yang digunakan membawa data agar dapat digunakan untuk berpindah platform menjadi lebih mudah.